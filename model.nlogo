turtles-own [
  marginalized ; whether or not an agent is marginalized
  c1 ; agreement to the statement "microaggressions do not constitute a wrong"
  c2 ; agreement to the statement "members of the marginalized group are overly sensitive"
]

to setup
  clear-all

  ; initialize specified number of agents
  create-turtles population [
    ; generic properties
    set heading 0
    set size 2.5
    set color white

    ; non-marginalized properties
    set shape "plus"
    set marginalized false
    set c1 bounds (random-normal p_c1_mean p_c1_deviation)
    set c2 bounds (random-normal p_c2_mean p_c2_deviation)
  ]

  ; mark specified percentage of agents marginalized
  ask n-of floor(population * (margin_size / 100)) turtles [
    ; marginalized properties
    set shape "minus"
    set marginalized true
    set c1 bounds (random-normal m_c1_mean m_c1_deviation)
    set c2 bounds (random-normal m_c2_mean m_c2_deviation)
  ]

  ; color the world
  ask patches with [pxcor >= floor positive_threshold ] [ set pcolor green ]
  ask patches with [pxcor < floor positive_threshold and pxcor > floor negative_threshold ] [ set pcolor grey ]
  ask patches with [pxcor <= floor negative_threshold ] [ set pcolor red ]
  ask patches with [pxcor = floor action_threshold ] [ set pcolor black ]

  ; set positions
  ask turtles [ setxy c1 c2 ]

  reset-ticks
end

to load
  user-message (word
    "Note: This feature has not yet been fully implemented. "
    "You will be able to load a pre-configured scenario in the next step. "
    "This will set some configuration options to simulate a specific scenario "
    "observed in real-world society. Feel free to tweak the settings and observe "
    "how the model's behavior changes."
  )
  let scenario user-input "Select a scenario to load: Racism, Sexism, Transphobia"
  (ifelse
    scenario = "Racism" [
      ; TODO racism presets
      set margin_size 10
      set stealth 1
      setup
      user-message (word
        "The racism scenario observes..."
      )
    ]
    scenario = "Sexism" [
      ; TODO sexism presets
      set margin_size 50
      set stealth 1
      setup
      user-message (word
        "The sexism scenario observes..."
      )
    ]
    scenario = "Transphobia" [
      ; TODO transphobia presets
      set margin_size 3
      set stealth 33.3
      setup
      user-message (word
        "The transphobia scenario observes..."
      )
    ]
  )
end

; enforce [0, 100] bounds
to-report bounds [number]
  report median (list 0 number 100)
end

; return new conviction value with change applied
to-report conviction_change [conviction change]
  report bounds (conviction + change)
  ; TODO decide if we want to simply add the change values as we do now,
  ; or if we want to go back to the below multiplication approach.
  ;report (ifelse-value
  ;  (change = 0) [ conviction ]
  ;  (conviction = 0 and change > 0) [ 1 ]
  ;  [ bounds (conviction * (1 + (change * 0.01 ))) ]
  ;)
end

; individual poisson-distributed thresholds, see HOW IT WORKS
to-report individual_action_threshold
  report random-poisson (action_threshold + (100 - action_threshold) / 2)
end
to-report individual_positive_threshold
  report random-poisson (positive_threshold + (100 - positive_threshold) / 2)
end
to-report individual_negative_threshold
  report random-poisson ((negative_threshold) / 2)
end

to-report unlinked
  report turtles with [not any? link-neighbors ]
end

to go
  ; get rid of old links and reset colors
  ask links [ die ]
  ask turtles [
    set color white
    set label ""
  ]

  while [count unlinked > 2] [
    ask one-of unlinked [
      create-link-to one-of other unlinked [
        ifelse [c1] of end1 >= individual_action_threshold [
          ask end1 [ set color red ]
          let end1_clocked (random 100 > stealth and [marginalized] of end1)
          let end2_clocked (random 100 > stealth and [marginalized] of end2)
          set shape "big_arrow"
          set thickness .666
          (ifelse
            [ c1 ] of end2 >= individual_positive_threshold [
              ; -------------------- positive reaction --------------------
              set color pink
              ask end2 [
                ifelse marginalized [
                  ifelse end1_clocked [
                    set c1 conviction_change(c1)(m_c1_on_positive_to_m)
                    set c2 conviction_change(c2)(m_c2_on_positive_to_m)
                  ] [
                    set c1 conviction_change(c1)(m_c1_on_positive_to_p)
                    set c2 conviction_change(c2)(m_c2_on_positive_to_p)
                  ]
                ] [
                  ifelse end1_clocked [
                    set c1 conviction_change(c1)(p_c1_on_positive_to_m)
                    set c2 conviction_change(c2)(p_c2_on_positive_to_m)
                  ] [
                    set c1 conviction_change(c1)(p_c1_on_positive_to_p)
                    set c2 conviction_change(c2)(p_c2_on_positive_to_p)
                  ]
                ]
              ]
              ask end1 [
                ifelse marginalized [
                  ifelse end2_clocked [
                    set c1 conviction_change(c1)(m_c1_on_positive_from_m)
                    set c2 conviction_change(c2)(m_c2_on_positive_from_m)
                  ] [
                    set c1 conviction_change(c1)(m_c1_on_positive_from_p)
                    set c2 conviction_change(c2)(m_c2_on_positive_from_p)
                  ]
                ] [
                  ifelse end2_clocked [
                    set c1 conviction_change(c1)(p_c1_on_positive_from_m)
                    set c2 conviction_change(c2)(p_c2_on_positive_from_m)
                  ] [
                    set c1 conviction_change(c1)(p_c1_on_positive_from_p)
                    set c2 conviction_change(c2)(p_c2_on_positive_from_p)
                  ]
                ]
              ]
            ]
            [ c1 ] of end2 < individual_negative_threshold [
              ; -------------------- negative reaction --------------------
              set color yellow
              ask end2 [
                ifelse marginalized [
                  ifelse end1_clocked [
                    set c1 conviction_change(c1)(m_c1_on_negative_to_m)
                    set c2 conviction_change(c2)(m_c2_on_negative_to_m)
                  ] [
                    set c1 conviction_change(c1)(m_c1_on_negative_to_p)
                    set c2 conviction_change(c2)(m_c2_on_negative_to_p)
                  ]
                ] [
                  ifelse end1_clocked [
                    set c1 conviction_change(c1)(p_c1_on_negative_to_m)
                    set c2 conviction_change(c2)(p_c2_on_negative_to_m)
                  ] [
                    set c1 conviction_change(c1)(p_c1_on_negative_to_p)
                    set c2 conviction_change(c2)(p_c2_on_negative_to_p)
                  ]
                ]
              ]
              ask end1 [
                ifelse random 100 > critical_faculty [
                  set label "reject"
                  ifelse marginalized [
                    ifelse end2_clocked [
                      set c1 conviction_change(c1)(m_c1_on_negative_rejected_from_m)
                      set c2 conviction_change(c2)(m_c2_on_negative_rejected_from_m)
                    ] [
                      set c1 conviction_change(c1)(m_c1_on_negative_rejected_from_p)
                      set c2 conviction_change(c2)(m_c2_on_negative_rejected_from_p)
                    ]
                  ] [
                    ifelse end2_clocked [
                      set c1 conviction_change(c1)(p_c1_on_negative_rejected_from_m)
                      set c2 conviction_change(c2)(p_c2_on_negative_rejected_from_m)
                    ] [
                      set c1 conviction_change(c1)(p_c1_on_negative_rejected_from_p)
                      set c2 conviction_change(c2)(p_c2_on_negative_rejected_from_p)
                    ]
                  ]
                ] [
                  set label "accept"
                  ifelse marginalized [
                    ifelse end2_clocked [
                      set c1 conviction_change(c1)(m_c1_on_negative_accepted_from_m)
                      set c2 conviction_change(c2)(m_c2_on_negative_accepted_from_m)
                    ] [
                      set c1 conviction_change(c1)(m_c1_on_negative_accepted_from_p)
                      set c2 conviction_change(c2)(m_c2_on_negative_accepted_from_p)
                    ]
                  ] [
                    ifelse end2_clocked [
                      set c1 conviction_change(c1)(p_c1_on_negative_accepted_from_m)
                      set c2 conviction_change(c2)(p_c2_on_negative_accepted_from_m)
                    ] [
                      set c1 conviction_change(c1)(p_c1_on_negative_accepted_from_p)
                      set c2 conviction_change(c2)(p_c2_on_negative_accepted_from_p)
                    ]
                  ]
                ]
              ]
            ]
            [
              ; -------------------- neutral reaction --------------------
              set color blue
              ask end2 [
                ifelse marginalized [
                  ifelse end1_clocked [
                    set c1 conviction_change(c1)(m_c1_on_neutral_to_m)
                    set c2 conviction_change(c2)(m_c2_on_neutral_to_m)
                  ] [
                    set c1 conviction_change(c1)(m_c1_on_neutral_to_p)
                    set c2 conviction_change(c2)(m_c2_on_neutral_to_p)
                  ]
                ] [
                  ifelse end1_clocked [
                    set c1 conviction_change(c1)(p_c1_on_neutral_to_m)
                    set c2 conviction_change(c2)(p_c2_on_neutral_to_m)
                  ] [
                    set c1 conviction_change(c1)(p_c1_on_neutral_to_p)
                    set c2 conviction_change(c2)(p_c2_on_neutral_to_p)
                  ]
                ]
              ]
              ask end1 [
                ifelse marginalized [
                  ifelse end2_clocked [
                    set c1 conviction_change(c1)(m_c1_on_neutral_from_m)
                    set c2 conviction_change(c2)(m_c2_on_neutral_from_m)
                  ] [
                    set c1 conviction_change(c1)(m_c1_on_neutral_from_p)
                    set c2 conviction_change(c2)(m_c2_on_neutral_from_p)
                  ]
                ] [
                  ifelse end2_clocked [
                    set c1 conviction_change(c1)(p_c1_on_neutral_from_m)
                    set c2 conviction_change(c2)(p_c2_on_neutral_from_m)
                  ] [
                    set c1 conviction_change(c1)(p_c1_on_neutral_from_p)
                    set c2 conviction_change(c2)(p_c2_on_neutral_from_p)
                  ]
                ]
              ]
            ]
          )
        ] [
          ; all non-interacting agents' convictions decay
          hide-link
          ask both-ends [
            ifelse marginalized [
              set c1 conviction_change(c1)(m_c1_on_idle)
              set c2 conviction_change(c2)(m_c2_on_idle)
            ] [
              set c1 conviction_change(c1)(p_c1_on_idle)
              set c2 conviction_change(c2)(p_c2_on_idle)
            ]
          ]
        ]
      ]
    ]
  ]

  ask turtles [
    ifelse marginalized [
      set c1 conviction_change(c1)(random-normal m_c1_noise_mean m_c1_noise_deviation)
      set c2 conviction_change(c2)(random-normal m_c2_noise_mean m_c2_noise_deviation)
    ] [
      set c1 conviction_change(c1)(random-normal p_c1_noise_mean p_c1_noise_deviation)
      set c2 conviction_change(c2)(random-normal p_c2_noise_mean p_c2_noise_deviation)
    ]
  ]

  ask turtles [ setxy c1 c2 ]
  ; check end conditions
  (ifelse
    not any? turtles with [ c1 >= action_threshold ] [
      clear-output
      output-print "equilibrium: no potential perpetrators"
      stop
    ]
    not any? turtles with [ c1 <= negative_threshold ] [
      clear-output
      output-print "equilibrium: no negative reactors"
      stop
    ]
    ; fixme deadlock detection doesn't work
    not any? turtles with [ c1 < 100 - ((100 - positive_threshold) * .9) ] with [c1 != 0] [
      clear-output
      output-print "deadlock: society is too polarized for change"
      stop
    ]
    [
      tick
    ]
  )
end
@#$#@#$#@
GRAPHICS-WINDOW
210
225
748
764
-1
-1
5.25
1
10
1
1
1
0
0
0
1
0
100
0
100
0
0
1
ticks
30.0

BUTTON
20
40
100
73
NIL
setup
NIL
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

BUTTON
110
40
190
73
NIL
go
T
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

MONITOR
20
240
190
285
agents
sentence\ncount turtles with [ marginalized = false ]\n\"+\"\ncount turtles with [ marginalized = true ]\n\"=\"\ncount turtles
0
1
11

PLOT
770
40
1310
360
convictions
time
% agreement
0.0
5.0
0.0
100.0
true
true
"" ""
PENS
"c1" 1.0 0 -7500403 true "" "plot mean [ c1 ] of turtles"
"c2" 1.0 0 -955883 true "" "plot mean [ c2 ] of turtles"
"action_threshold" 1.0 0 -16777216 true "" "plot action_threshold"
"positive_threshold" 1.0 0 -10899396 true "" "plot positive_threshold"
"negative_threshold" 1.0 0 -2674135 true "" "plot negative_threshold"

SLIDER
20
160
190
193
population
population
1
1000
250.0
1
1
agents
HORIZONTAL

SLIDER
20
200
190
233
margin_size
margin_size
0
100
10.0
0.1
1
%
HORIZONTAL

SLIDER
370
940
715
973
p_c2_on_idle
p_c2_on_idle
-100
100
-0.1
.1
1
%
HORIZONTAL

SLIDER
20
745
190
778
critical_faculty
critical_faculty
0
100
100.0
.1
1
%
HORIZONTAL

SLIDER
20
365
190
398
action_threshold
action_threshold
negative_threshold
100
66.6
.1
1
%
HORIZONTAL

SLIDER
20
535
190
568
positive_threshold
positive_threshold
negative_threshold
100
50.0
.1
1
%
HORIZONTAL

SLIDER
20
625
190
658
negative_threshold
negative_threshold
0
positive_threshold
15.0
.1
1
%
HORIZONTAL

SLIDER
210
40
380
73
p_c1_mean
p_c1_mean
0
100
45.0
.1
1
%
HORIZONTAL

SLIDER
210
80
380
113
p_c1_deviation
p_c1_deviation
0
100
20.0
.1
1
%
HORIZONTAL

SLIDER
580
40
750
73
m_c1_mean
m_c1_mean
0
100
20.0
.1
1
%
HORIZONTAL

SLIDER
580
80
750
113
m_c1_deviation
m_c1_deviation
0
100
20.0
.1
1
%
HORIZONTAL

SLIDER
210
120
380
153
p_c2_mean
p_c2_mean
0
100
33.3
.1
1
%
HORIZONTAL

SLIDER
210
160
380
193
p_c2_deviation
p_c2_deviation
0
100
33.3
.1
1
%
HORIZONTAL

SLIDER
580
120
750
153
m_c2_mean
m_c2_mean
0
100
1.0
.1
1
%
HORIZONTAL

SLIDER
580
160
750
193
m_c2_deviation
m_c2_deviation
0
100
1.0
.1
1
%
HORIZONTAL

SLIDER
20
785
190
818
stealth
stealth
0
100
1.0
.1
1
%
HORIZONTAL

MONITOR
20
315
190
360
potential perpetrators
sentence\ncount turtles with [ marginalized = false and c1 >= action_threshold ]\n\"+\"\ncount turtles with [ marginalized = true and c1 >= action_threshold ]\n\"=\"\ncount turtles with [ c1 >= action_threshold ]
0
1
11

MONITOR
20
485
190
530
positive reactors
sentence\ncount turtles with [ marginalized = false and c1 >= positive_threshold ]\n\"+\"\ncount turtles with [ marginalized = true and c1 >= positive_threshold ]\n\"=\"\ncount turtles with [ c1 >= positive_threshold ]
0
1
11

MONITOR
20
575
190
620
neutral reactors
sentence\ncount turtles with [ marginalized = false and c1 < positive_threshold and c1 > negative_threshold ]\n\"+\"\ncount turtles with [ marginalized = true and c1 < positive_threshold and c1 > negative_threshold ]\n\"=\"\ncount turtles with [ c1 < positive_threshold and c1 > negative_threshold ]
0
1
11

MONITOR
20
665
190
710
negative reactors
sentence\ncount turtles with [ marginalized = false and c1 <= negative_threshold ]\n\"+\"\ncount turtles with [ marginalized = true and c1 <= negative_threshold ]\n\"=\"\ncount turtles with [ c1 <= negative_threshold ]
17
1
11

MONITOR
20
405
190
450
non-perpetrators
sentence\ncount turtles with [ marginalized = false and c1 < action_threshold ]\n\"+\"\ncount turtles with [ marginalized = true and c1 < action_threshold ]\n\"=\"\ncount turtles with [ c1 < action_threshold ]
17
1
11

TEXTBOX
20
20
190
46
__general controls_________
12
0.0
1

TEXTBOX
210
200
750
218
__simulation_____________________________________________________________________________
12
0.0
1

TEXTBOX
20
725
190
751
__misc controls____________
12
0.0
1

TEXTBOX
20
465
195
491
__reaction behavior________
12
0.0
1

TEXTBOX
20
295
200
321
__action behavior__________
12
0.0
1

TEXTBOX
210
20
750
46
__conviction initialization________________________________________________________________
12
0.0
1

SLIDER
20
940
365
973
p_c1_on_idle
p_c1_on_idle
-100
100
-0.1
.1
1
%
HORIZONTAL

TEXTBOX
770
20
1325
61
__conviction plot_________________________________________________________________________
12
0.0
1

PLOT
1330
40
1870
360
reactions
time
% of agents
0.0
5.0
0.0
100.0
true
true
"" ""
PENS
"positive" 1.0 0 -10899396 true "" "plot (count turtles with [ c1 >= positive_threshold ] / population) * 100"
"neutral" 1.0 0 -7500403 true "" "plot (count turtles with [ c1 < positive_threshold and c1 > negative_threshold ] / population) * 100"
"negative" 1.0 0 -2674135 true "" "plot (count turtles with [ c1 <= negative_threshold ] / population) * 100"

PLOT
770
625
1870
820
actions
time
%
0.0
5.0
0.0
100.0
true
true
"" ""
PENS
"potential perpetrators" 1.0 0 -2674135 true "" "plot (count turtles with [ c1 >= action_threshold ] / population) * 100"
"non-perpetrators" 1.0 0 -7500403 true "" "plot (count turtles with [ c1 < action_threshold ] / population) * 100"
"share of marginalized pot. perpetrators" 1.0 0 -955883 true "" "plot ((count turtles with [ c1 >= action_threshold and marginalized = true]) / (count turtles with [ c1 >= action_threshold])) * 100"

TEXTBOX
1330
20
1875
61
__reaction plot___________________________________________________________________________
12
0.0
1

PLOT
770
370
1030
530
non-marginalized convictions
time
%
0.0
5.0
0.0
100.0
true
false
"" ""
PENS
"c1" 1.0 0 -7500403 true "" "plot mean [ c1 ] of turtles with [marginalized = false]"
"c2" 1.0 0 -955883 true "" "plot mean [ c2 ] of turtles with [marginalized = false]"

PLOT
1050
370
1310
530
marginalized convictions
time
%
0.0
5.0
0.0
100.0
true
false
"" ""
PENS
"c1" 1.0 0 -7500403 true "" "plot mean [ c1 ] of turtles with [marginalized = true]"
"c2" 1.0 0 -955883 true "" "plot mean [ c2 ] of turtles with [marginalized = true]"

PLOT
1330
370
1590
530
non-marginalized reactions
time
%
0.0
5.0
0.0
100.0
true
false
"" ""
PENS
"positive" 1.0 0 -10899396 true "" "plot (count turtles with [ c1 >= positive_threshold and marginalized = false] / count turtles with [marginalized = false]) * 100"
"neutral" 1.0 0 -7500403 true "" "plot (count turtles with [ c1 < positive_threshold and c1 > negative_threshold and marginalized = false] / count turtles with [marginalized = false]) * 100"
"pen-2" 1.0 0 -2674135 true "" "plot (count turtles with [ c1 <= negative_threshold and marginalized = false] / count turtles with [marginalized = false]) * 100"

PLOT
1610
370
1870
530
marginalized reactions
time
%
0.0
5.0
0.0
100.0
true
false
"" ""
PENS
"positive" 1.0 0 -10899396 true "" "plot (count turtles with [ c1 >= positive_threshold and marginalized = true] / count turtles with [ marginalized = true ]) * 100"
"neutral" 1.0 0 -7500403 true "" "plot (count turtles with [ c1 < positive_threshold and c1 > negative_threshold and marginalized = true] / count turtles with [marginalized = true]) * 100"
"negative" 1.0 0 -2674135 true "" "plot (count turtles with [ c1 <= negative_threshold and marginalized = true] / count turtles with [marginalized = true]) * 100"

MONITOR
770
540
900
585
c1
mean [ c1 ] of turtles with [marginalized = false]
2
1
11

MONITOR
910
540
1030
585
c2
mean [ c2 ] of turtles with [marginalized = false]
2
1
11

MONITOR
1050
540
1170
585
c1
mean [ c1 ] of turtles with [marginalized = true]
2
1
11

MONITOR
1180
540
1310
585
c2
mean [ c2 ] of turtles with [marginalized = true]
2
1
11

MONITOR
1330
540
1415
585
positive
(count turtles with [ c1 >= positive_threshold and marginalized = false] / count turtles with [marginalized = false]) * 100
2
1
11

MONITOR
1425
540
1500
585
neutral
(count turtles with [ c1 < positive_threshold and c1 > negative_threshold and marginalized = false] / count turtles with [marginalized = false]) * 100
2
1
11

MONITOR
1510
540
1590
585
negative
(count turtles with [ c1 <= negative_threshold and marginalized = false] / count turtles with [marginalized = false]) * 100
2
1
11

MONITOR
1610
540
1690
585
positive
(count turtles with [ c1 >= positive_threshold and marginalized = true] / count turtles with [ marginalized = true ]) * 100
2
1
11

MONITOR
1700
540
1775
585
neutral
(count turtles with [ c1 < positive_threshold and c1 > negative_threshold and marginalized = true] / count turtles with [marginalized = true]) * 100
2
1
11

MONITOR
1785
540
1870
585
negative
(count turtles with [ c1 <= negative_threshold and marginalized = true] / count turtles with [marginalized = true]) * 100
2
1
11

TEXTBOX
770
605
1875
631
__actions plot_________________________________________________________________________________________________________________________________________________________________________
12
0.0
1

TEXTBOX
400
60
570
90
c1: \"microaggressions do not constitute a wrong\"
12
0.0
1

TEXTBOX
400
140
570
175
c2: \"marginalized agents are overly sensitive\"
12
0.0
1

SLIDER
770
940
1115
973
m_c1_on_idle
m_c1_on_idle
-100
100
-0.1
.1
1
%
HORIZONTAL

SLIDER
1120
940
1465
973
m_c2_on_idle
m_c2_on_idle
-100
100
-0.1
.1
1
%
HORIZONTAL

SLIDER
20
1010
365
1043
p_c1_on_positive_from_p
p_c1_on_positive_from_p
-100
100
5.0
.1
1
%
HORIZONTAL

SLIDER
20
1195
365
1228
p_c1_on_neutral_from_p
p_c1_on_neutral_from_p
-100
100
2.5
.1
1
%
HORIZONTAL

SLIDER
20
1380
365
1413
p_c1_on_negative_accepted_from_p
p_c1_on_negative_accepted_from_p
-100
100
-10.0
.1
1
%
HORIZONTAL

SLIDER
20
1460
365
1493
p_c1_on_negative_rejected_from_p
p_c1_on_negative_rejected_from_p
-100
100
15.0
.1
1
%
HORIZONTAL

OUTPUT
210
770
750
820
12

SLIDER
20
1090
365
1123
p_c1_on_positive_to_p
p_c1_on_positive_to_p
-100
100
2.5
.1
1
%
HORIZONTAL

SLIDER
20
1275
365
1308
p_c1_on_neutral_to_p
p_c1_on_neutral_to_p
-100
100
1.0
.1
1
%
HORIZONTAL

SLIDER
20
1540
365
1573
p_c1_on_negative_to_p
p_c1_on_negative_to_p
-100
100
-5.0
.1
1
%
HORIZONTAL

TEXTBOX
20
915
1480
941
__idle reactions_________________________________________________________________________________________________________________________________________________________________________________________________________________________________
12
0.0
1

SLIDER
770
1010
1115
1043
m_c1_on_positive_from_p
m_c1_on_positive_from_p
-100
100
5.0
.1
1
%
HORIZONTAL

SLIDER
770
1090
1115
1123
m_c1_on_positive_to_p
m_c1_on_positive_to_p
-100
100
2.5
.1
1
%
HORIZONTAL

SLIDER
770
1195
1115
1228
m_c1_on_neutral_from_p
m_c1_on_neutral_from_p
-100
100
2.5
.1
1
%
HORIZONTAL

SLIDER
770
1275
1115
1308
m_c1_on_neutral_to_p
m_c1_on_neutral_to_p
-100
100
1.0
.1
1
%
HORIZONTAL

SLIDER
770
1380
1115
1413
m_c1_on_negative_accepted_from_p
m_c1_on_negative_accepted_from_p
-100
100
-10.0
.1
1
%
HORIZONTAL

SLIDER
770
1460
1115
1493
m_c1_on_negative_rejected_from_p
m_c1_on_negative_rejected_from_p
-100
100
15.0
.1
1
%
HORIZONTAL

SLIDER
770
1540
1115
1573
m_c1_on_negative_to_p
m_c1_on_negative_to_p
-100
100
-5.0
.1
1
%
HORIZONTAL

BUTTON
20
120
190
153
load scenario...
load
NIL
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

BUTTON
110
80
190
113
go once
go
NIL
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

SLIDER
1120
1010
1465
1043
m_c2_on_positive_from_p
m_c2_on_positive_from_p
-100
100
0.0
.1
1
%
HORIZONTAL

SLIDER
1120
1045
1465
1078
m_c2_on_positive_from_m
m_c2_on_positive_from_m
-100
100
-0.0
.1
1
%
HORIZONTAL

SLIDER
1120
1090
1465
1123
m_c2_on_positive_to_p
m_c2_on_positive_to_p
-100
100
0.0
.1
1
%
HORIZONTAL

SLIDER
1120
1125
1465
1158
m_c2_on_positive_to_m
m_c2_on_positive_to_m
-100
100
0.0
.1
1
%
HORIZONTAL

SLIDER
1120
1195
1465
1228
m_c2_on_neutral_from_p
m_c2_on_neutral_from_p
-100
100
0.0
.1
1
%
HORIZONTAL

SLIDER
1120
1230
1465
1263
m_c2_on_neutral_from_m
m_c2_on_neutral_from_m
-100
100
0.0
.1
1
%
HORIZONTAL

SLIDER
1120
1275
1465
1308
m_c2_on_neutral_to_p
m_c2_on_neutral_to_p
-100
100
-0.0
.1
1
%
HORIZONTAL

SLIDER
1120
1310
1465
1343
m_c2_on_neutral_to_m
m_c2_on_neutral_to_m
-100
100
-0.0
.1
1
%
HORIZONTAL

SLIDER
1120
1380
1465
1413
m_c2_on_negative_accepted_from_p
m_c2_on_negative_accepted_from_p
-100
100
-10.0
.1
1
%
HORIZONTAL

SLIDER
1120
1415
1465
1448
m_c2_on_negative_accepted_from_m
m_c2_on_negative_accepted_from_m
-100
100
-50.0
.1
1
%
HORIZONTAL

SLIDER
1120
1460
1465
1493
m_c2_on_negative_rejected_from_p
m_c2_on_negative_rejected_from_p
-100
100
0.0
.1
1
%
HORIZONTAL

SLIDER
1120
1495
1465
1528
m_c2_on_negative_rejected_from_m
m_c2_on_negative_rejected_from_m
-100
100
50.0
.1
1
%
HORIZONTAL

SLIDER
1120
1540
1465
1573
m_c2_on_negative_to_p
m_c2_on_negative_to_p
-100
100
-10.0
.1
1
%
HORIZONTAL

SLIDER
1120
1575
1465
1608
m_c2_on_negative_to_m
m_c2_on_negative_to_m
-100
100
-50.0
.1
1
%
HORIZONTAL

SLIDER
370
1010
715
1043
p_c2_on_positive_from_p
p_c2_on_positive_from_p
-100
100
0.0
.1
1
%
HORIZONTAL

SLIDER
370
1045
715
1078
p_c2_on_positive_from_m
p_c2_on_positive_from_m
-100
100
0.0
.1
1
%
HORIZONTAL

SLIDER
370
1090
715
1123
p_c2_on_positive_to_p
p_c2_on_positive_to_p
-100
100
0.0
.1
1
%
HORIZONTAL

SLIDER
370
1125
715
1158
p_c2_on_positive_to_m
p_c2_on_positive_to_m
-100
100
0.0
.1
1
%
HORIZONTAL

SLIDER
370
1195
715
1228
p_c2_on_neutral_from_p
p_c2_on_neutral_from_p
-100
100
0.0
.1
1
%
HORIZONTAL

SLIDER
370
1230
715
1263
p_c2_on_neutral_from_m
p_c2_on_neutral_from_m
-100
100
0.0
.1
1
%
HORIZONTAL

SLIDER
370
1275
715
1308
p_c2_on_neutral_to_p
p_c2_on_neutral_to_p
-100
100
0.0
.1
1
%
HORIZONTAL

SLIDER
370
1310
715
1343
p_c2_on_neutral_to_m
p_c2_on_neutral_to_m
-100
100
0.0
.1
1
%
HORIZONTAL

SLIDER
370
1380
715
1413
p_c2_on_negative_accepted_from_p
p_c2_on_negative_accepted_from_p
-100
100
-10.0
.1
1
%
HORIZONTAL

SLIDER
370
1415
715
1448
p_c2_on_negative_accepted_from_m
p_c2_on_negative_accepted_from_m
-100
100
-50.0
.1
1
%
HORIZONTAL

SLIDER
370
1460
715
1493
p_c2_on_negative_rejected_from_p
p_c2_on_negative_rejected_from_p
-100
100
0.0
.1
1
%
HORIZONTAL

SLIDER
370
1495
715
1528
p_c2_on_negative_rejected_from_m
p_c2_on_negative_rejected_from_m
-100
100
50.0
.1
1
%
HORIZONTAL

SLIDER
370
1540
715
1573
p_c2_on_negative_to_p
p_c2_on_negative_to_p
-100
100
-10.0
.1
1
%
HORIZONTAL

SLIDER
370
1575
715
1608
p_c2_on_negative_to_m
p_c2_on_negative_to_m
-100
100
-50.0
.1
1
%
HORIZONTAL

TEXTBOX
20
985
1505
1003
__positive reactions_____________________________________________________________________________________________________________________________________________________________________________________________________________________________
12
0.0
1

TEXTBOX
20
1170
1495
1188
__neutral reactions_____________________________________________________________________________________________________________________________________________________________________________________________________________________________
12
0.0
1

TEXTBOX
20
1355
1475
1381
__negative reactions____________________________________________________________________________________________________________________________________________________________________________________________________________________________
12
0.0
1

SLIDER
20
1045
365
1078
p_c1_on_positive_from_m
p_c1_on_positive_from_m
-100
100
5.0
.1
1
%
HORIZONTAL

SLIDER
20
1125
365
1158
p_c1_on_positive_to_m
p_c1_on_positive_to_m
-100
100
2.5
.1
1
%
HORIZONTAL

SLIDER
20
1230
365
1263
p_c1_on_neutral_from_m
p_c1_on_neutral_from_m
-100
100
2.5
.1
1
%
HORIZONTAL

SLIDER
20
1310
365
1343
p_c1_on_neutral_to_m
p_c1_on_neutral_to_m
-100
100
2.0
.1
1
%
HORIZONTAL

SLIDER
20
1415
365
1448
p_c1_on_negative_accepted_from_m
p_c1_on_negative_accepted_from_m
-100
100
-10.0
.1
1
%
HORIZONTAL

SLIDER
20
1495
365
1528
p_c1_on_negative_rejected_from_m
p_c1_on_negative_rejected_from_m
-100
100
30.0
.1
1
%
HORIZONTAL

SLIDER
20
1575
365
1608
p_c1_on_negative_to_m
p_c1_on_negative_to_m
-100
100
-10.0
.1
1
%
HORIZONTAL

SLIDER
770
1045
1115
1078
m_c1_on_positive_from_m
m_c1_on_positive_from_m
-100
100
5.0
.1
1
%
HORIZONTAL

SLIDER
770
1125
1115
1158
m_c1_on_positive_to_m
m_c1_on_positive_to_m
-100
100
2.5
.1
1
%
HORIZONTAL

SLIDER
770
1230
1115
1263
m_c1_on_neutral_from_m
m_c1_on_neutral_from_m
-100
100
2.5
.1
1
%
HORIZONTAL

SLIDER
770
1310
1115
1343
m_c1_on_neutral_to_m
m_c1_on_neutral_to_m
-100
100
2.0
.1
1
%
HORIZONTAL

SLIDER
770
1415
1115
1448
m_c1_on_negative_accepted_from_m
m_c1_on_negative_accepted_from_m
-100
100
-10.0
.1
1
%
HORIZONTAL

SLIDER
770
1495
1115
1528
m_c1_on_negative_rejected_from_m
m_c1_on_negative_rejected_from_m
-100
100
30.0
.1
1
%
HORIZONTAL

SLIDER
770
1575
1115
1608
m_c1_on_negative_to_m
m_c1_on_negative_to_m
-100
100
-10.0
.1
1
%
HORIZONTAL

BUTTON
20
80
100
113
go 5x
repeat 5 [ go wait 1 ]
NIL
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

SLIDER
1525
975
1870
1008
m_c1_noise_deviation
m_c1_noise_deviation
0
100
1.5
.1
1
%
HORIZONTAL

TEXTBOX
1525
915
1880
941
__noise__________________________________________________
12
0.0
1

SLIDER
1525
940
1870
973
m_c1_noise_mean
m_c1_noise_mean
-100
100
0.0
.1
1
%
HORIZONTAL

SLIDER
1525
1020
1870
1053
m_c2_noise_mean
m_c2_noise_mean
-100
100
0.0
.1
1
%
HORIZONTAL

SLIDER
1525
1055
1870
1088
m_c2_noise_deviation
m_c2_noise_deviation
0
100
1.0
.1
1
%
HORIZONTAL

SLIDER
1525
1100
1870
1133
p_c1_noise_mean
p_c1_noise_mean
-100
100
0.0
.1
1
%
HORIZONTAL

SLIDER
1525
1135
1870
1168
p_c1_noise_deviation
p_c1_noise_deviation
0
100
1.5
.1
1
%
HORIZONTAL

SLIDER
1525
1180
1870
1213
p_c2_noise_mean
p_c2_noise_mean
-100
100
0.0
.1
1
%
HORIZONTAL

SLIDER
1525
1215
1870
1248
p_c2_noise_deviation
p_c2_noise_deviation
0
100
1.0
.1
1
%
HORIZONTAL

@#$#@#$#@
## WHAT IS IT?

Members of marginalized communities are often accused of being "too sensitive" when subjected to supposedly harmless acts of microaggression. This model simulates a society consisting of marginalized and non-marginalized agents who interact and may or may not perform acts of microaggression based on their convictions. Agents witnessing a microaggression might condone, ignore or condemn such microaggressions, thus potentially influencing a perpetrator's conviction. The model has been discussed in a [paper published on arXiv](https://doi.org/10.48550/arXiv.2207.01017).

## HOW IT WORKS

<!-- what rules create the overall behavior of the model -->

Marginalized and Non-Marginalized agents can be initialized with normal-distributed agreement percentages for two convictions:

- `c1`: "microaggressions do not constitute a wrong"
- `c2`: "members of the marginalized group are overly sensitive"

Agents can interact with one another 1:1 and may commit microaggressions in these interactions. An agent witnessing a microaggression may react positively, negatively, or neutrally. This behavior is configured through thresholds that control Poisson-distributed probability values calculated for every interaction.

Suppose the `action_threshold` is set to 75%. All agents with `c1 >= 75` may now theoretically become perpetrators. Whether or not an agent actually commits a microaggression is determined by whether or not their `c1 >= individual_action_threshold`, where `individual_action_threshold` is a poisson-distributed probability variable with a mean of `(action_threshold + (100 - action_threshold) / 2) = 75 + 12.5 = 87.5`. Thus, the higher the `c1` of an agent, the higher their chance of becoming a perpetrator.

Reactions are Poisson-distributed as well. Positive reactions are controlled by the `individual_positive_threshold`, a Poisson-distributed probability variable with a mean of `(positive_threshold + (100 - positive_threshold) / 2)`; halfway between `positive_threshold` and `100`. Negative reactions are controlled by the `individual_negative_threshold`, a Poisson-distributed probability variable with a mean of `((negative_threshold) / 2)`; halfway between `0` and `negative_threshold`.

## HOW TO USE IT

<!-- how to use the model, including a description of each of the items in the Interface tab -->

### general controls

The `setup` button will initialize a society based on the current configuration, `go` will start the simulation. The `go once` and `go 5x` buttons can be used for step-by-step execution. Pre-configured scenarios can be loaded using the `load scenario...` button. Every scenario is explained after selection.

Sliders `population` and `margin_size` control the number of agents and the percentage of marginalized agents.

### action behavior

The `action_threshold` slider controls the minimum required `c1` for an agent to commit a microaggression.

### reaction behavior

The `positive_threshold` controls the minimum required `c1` for an agent to react positively to a microaggression. Similarly, the `negative_threshold` specifies the maximum allowed `c1` for an agent to react negatively to a microaggression.

### [non_marginalized + marginalized = total] monitors

Monitors reporting values in the form of `[non_marginalized + marginalized = total]` are used across the UI to show how many agents a specific claim applies to; overall and broken down by whether or not they are marginalized.

### misc controls

The `critical_faculty` slider allows setting the likelihood of a perpetrator accepting criticism when made aware of a microaggression they committed. The `stealth` slider enables the simulation of types of marginalization that are less recognizable to other agents by specifying the likelihood of an agent not recognizing another agent as marginalized.

### conviction initialization

Convictions one (`c1`) and two (`c2`) for marginalized (`m`) and non-marginalized (`p`) agents are initialized as normal-distributed values with a `mean` and a `deviation` using sliders in the form of `<p|m>_<c1|c2>_<deviation|mean>`.

### simulation

Agents are placed on the canvas following the rules described in the **THINGS TO NOTICE** section. The below output field is used to display stop conditions for the simulation, such as a society that is too polarized for change or has no possible perpetrators.

### plots

The conviction plots depict the average agreement with `c1` and `c2` in society as a whole, among non-marginalized agents, and among marginalized agents.

The reaction plots depict the percentage of agents above the positive threshold (potential positive reactors), below the negative threshold (potential negative reactors), and in between the thresholds (neutral reactors). The reaction plots are similarly broken down by marginalization status.

The actions plot depicts the percentage of potential perpetrators, the percentage of non-perpetrators, and the share of marginalized agents among the potential perpetrators.

### reactions

Changes in convictions one (`c1`) and two (`c2`) due to interactions can be set individually for marginalized (`m`) and non-marginalized (`p`) agents. If no microaggression occurs in an interaction, the `idle` value is applied. Otherwise, a `positive`, `neutral`, or `negative` reaction might be given `to` or received `from` a marginalized (`m`) or non-marginalized (`p`) agent. A received `negative` reaction might be `accepted` or `rejected`. Sliders take the form `<p|m>_<c1|c2>_on_<idle|<<positive_<to|from>|neutral_<to|from>|negative_<to|accepted_from|rejected_from>>_<p|m>>`.

Example: A privileged agent, bob, commits a microaggression in an interaction with a marginalized agent, alice, who reacts neutrally. Bob's convictions change based on `p_c1_on_neutral_from_m` and `p_c2_on_neutral_from_m` respectively, while Alice's convictions change based on `m_c1_on_neutral_to_p` and `m_c2_on_neutral_to_p`.

### noise

Background noise changes to convictions one (`c1`) and two (`c2`) for marginalized (`m`) and non-marginalized (`p`) agents are modeled as normal-distributed values with a `mean` and a `deviation` using sliders in the form of `<p|m>_<c1|c2>_noise_<deviation|mean>`.

## THINGS TO NOTICE

<!-- suggested things for the user to notice while running the model -->

### World

- Based on their convictions, agents are mapped into a two-dimensional plane with the `x`-axis representing `c1` and the `y`-axis representing `c2`.
- The `action_threshold` is represented by a vertical black line. Agents right of this line are potential perpetrators.
- The green area represents the `positive_threshold`: Agents in this area might react positively to microaggressions.
- The red area represents the `negative_threshold`: Agents in this area might react negatively to microaggressions.
- Agents in the grey area will always react neutrally to microaggressions, as their `c1` is below the `positive_threshold` and above the `negative_threshold`.

### Agents

- Marginalized agents are represented by a **-**, non-marginalized agents by a **+**.
- If an agent is tinted red, they commit a microaggression in this tick. Otherwise, agents are tinted white.

### Interactions

- An arrow points from a microaggression perpetrator to their conversational partner.
- A pink arrow indicates that the conversational partner reacted positively to the microaggression.
- A blue arrow indicates that the conversational partner reacted neutrally to the microaggression.
- A yellow arrow indicates that the conversational partner reacted negatively to the microaggression.
- If a perpetrator accepts the criticism upon a negative reaction, they will receive the label "accept". Otherwise, they receive the label "reject".

## THINGS TO TRY

<!-- suggested things for the user to try to do (move sliders, switches, etc.) with the model -->

Three scenarios are shipped by default: Sexism, transphobia, and racism. Try loading each of these and see how the convictions behave differently over time.

## EXTENDING THE MODEL

<!-- suggested things to add or change in the Code tab to make the model more complicated, detailed, accurate, etc. -->

- More values could be random-distributed
- More scenarios could be pre-configured
- An agent's `c1` might influence their `critical_faculty`
- An agent's `c2` should be dependent on their `c1`
- Add a `false_positive_clock` likelihood slider analog to `stealth`

## NETLOGO FEATURES

<!--interesting or unusual features of NetLogo that the model uses, particularly in the Code tab; or where workarounds were needed for missing features-->

Creating proper 1:1 links in NetLogo can be pretty cumbersome, making it challenging to allow perpetrators to find targets not already linked. The following code will result in 1:2 or 1:1:1 relationships:

```
ask turtles with [ c1 >= action_threshold] [
  create-link-to one-of other turtles [ ... ]
]
```

A selection statement will not change this behavior since the condition is evaluated once before any links are created.

```
ask turtles with [ count link-neighbors = 0 and c1 >= action_threshold] [
  create-link-to one-of other turtles [ ... ]
]
```

The only workaround seems to be to iterate over every agent who needs a link neighbor instead of using `ask any`:

```
to-report unlinked
  report turtles with [not any? link-neighbors ]
end

to link-up
  while [count unlinked > 2] [
    ask one-of unlinked [
      create-link-to one-of other unlinked [
        ifelse [c1] of end1 >= individual_action_threshold [
          ; interaction goes here
        ] [
          hide-link ; hide unused links
        ]
      ]
    ]
  ]
end

```

However, this might be less performant and results in less readable code. See commit [`f57b7fcb27daed3ca6cffaab28d6273f97f71cfb`](https://gitlab.com/NeoTheThird/ceth-seminar/-/commit/f57b7fcb27daed3ca6cffaab28d6273f97f71cfb) for how this was implemented.

## RELATED MODELS

<!-- models in the NetLogo Models Library and elsewhere which are of related interest -->

No similar models are known to the authors at the time of writing. Hints are appreciated.

## CREDITS AND REFERENCES

This model has been the subject of the following paper published on arXiv:

- [Johannah Sprinz, 2022, **"Y'all are just too sensitive": A computational ethics approach to understanding how prejudice against marginalized communities becomes epistemic belief**, arXiv, doi: 10.48550/arXiv.2207.01017](https://doi.org/10.48550/arXiv.2207.01017)

The model is available [online in NetLogoWeb](https://netlogoweb.org/web?https://neothethird.gitlab.io/ceth-seminar/model.nlogo).

(c) [Matthias Fruth](https://de.linkedin.com/in/matthias-fruth-825711180) and [Johannah Sprinz](https://spri.nz/) 2022.

Original development within the [Master's practical seminar "Computational Ethics" at LMU Munich in the winter term of 2021](https://uni2work.ifi.lmu.de/course/W21/IfI/CEth), supervised by [Prof. Dr. François Bry](https://www.pms.ifi.lmu.de/mitarbeiter/derzeitige/francois-bry/index.html).

@#$#@#$#@
default
true
0
Polygon -7500403 true true 150 5 40 250 150 205 260 250

minus
true
0
Rectangle -7500403 true true 30 120 270 180

plus
true
0
Rectangle -7500403 true true 120 30 180 270
Rectangle -7500403 true true 30 120 270 180
@#$#@#$#@
NetLogo 6.2.2
@#$#@#$#@
@#$#@#$#@
@#$#@#$#@
<experiments>
  <experiment name="Margin_size" repetitions="10" runMetricsEveryStep="true">
    <setup>setup</setup>
    <go>go</go>
    <timeLimit steps="5000"/>
    <metric>mean [ c1 ] of turtles</metric>
    <metric>mean [ c2 ] of turtles</metric>
    <enumeratedValueSet variable="p_c1_on_neutral_to_m">
      <value value="2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="m_c2_on_neutral_from_p">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="stealth">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="m_c2_on_neutral_to_m">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="p_c2_on_positive_to_p">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="p_c2_on_positive_from_p">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="p_c2_on_idle">
      <value value="-0.1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="negative_threshold">
      <value value="15"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="m_c1_on_negative_to_m">
      <value value="-10"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="margin_size">
      <value value="0"/>
      <value value="10"/>
      <value value="20"/>
      <value value="30"/>
      <value value="40"/>
      <value value="50"/>
      <value value="60"/>
      <value value="70"/>
      <value value="80"/>
      <value value="90"/>
      <value value="100"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="p_c2_on_neutral_from_m">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="population">
      <value value="250"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="m_c1_on_neutral_to_p">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="p_c1_noise_mean">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="p_c1_on_positive_to_m">
      <value value="2.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="action_threshold">
      <value value="66.6"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="m_c1_on_positive_from_p">
      <value value="5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="p_c2_on_negative_accepted_from_p">
      <value value="-10"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="p_c2_on_negative_rejected_from_p">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="p_c2_mean">
      <value value="33.3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="p_c1_on_positive_from_p">
      <value value="5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="m_c1_on_negative_accepted_from_m">
      <value value="-10"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="m_c2_on_positive_to_m">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="m_c1_on_positive_to_p">
      <value value="2.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="p_c2_noise_deviation">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="m_c1_on_negative_rejected_from_m">
      <value value="30"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="m_c2_on_negative_accepted_from_p">
      <value value="-10"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="m_c1_on_neutral_from_m">
      <value value="2.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="m_c2_on_negative_rejected_from_p">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="m_c2_mean">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="p_c1_on_neutral_from_p">
      <value value="2.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="p_c1_on_negative_accepted_from_m">
      <value value="-10"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="p_c1_on_negative_rejected_from_m">
      <value value="30"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="m_c2_on_idle">
      <value value="-0.1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="m_c2_on_neutral_to_p">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="m_c2_on_positive_from_m">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="p_c2_on_negative_to_p">
      <value value="-10"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="m_c2_on_negative_to_m">
      <value value="-50"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="p_c2_on_neutral_from_p">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="p_c1_mean">
      <value value="45"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="m_c1_on_idle">
      <value value="-0.1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="p_c1_on_negative_to_m">
      <value value="-10"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="p_c2_on_positive_from_m">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="m_c1_noise_deviation">
      <value value="1.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="m_c2_noise_mean">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="m_c1_deviation">
      <value value="20"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="p_c2_on_neutral_to_m">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="m_c1_on_neutral_to_m">
      <value value="2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="m_c2_deviation">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="m_c1_mean">
      <value value="20"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="m_c1_on_positive_from_m">
      <value value="5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="p_c1_on_neutral_to_p">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="m_c1_noise_mean">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="m_c2_on_positive_to_p">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="m_c1_on_negative_to_p">
      <value value="-5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="m_c2_on_neutral_from_m">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="p_c1_on_positive_from_m">
      <value value="5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="p_c2_on_positive_to_m">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="p_c1_on_positive_to_p">
      <value value="2.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="m_c2_on_negative_accepted_from_m">
      <value value="-50"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="p_c1_on_negative_rejected_from_p">
      <value value="15"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="m_c2_on_negative_rejected_from_m">
      <value value="50"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="positive_threshold">
      <value value="50"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="m_c2_noise_deviation">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="p_c2_noise_mean">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="p_c2_on_negative_accepted_from_m">
      <value value="-50"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="critical_faculty">
      <value value="40"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="p_c2_on_negative_rejected_from_m">
      <value value="50"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="m_c1_on_negative_accepted_from_p">
      <value value="-10"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="p_c2_on_negative_to_m">
      <value value="-50"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="p_c1_on_negative_to_p">
      <value value="-5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="m_c1_on_negative_rejected_from_p">
      <value value="15"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="m_c1_on_neutral_from_p">
      <value value="2.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="p_c2_on_neutral_to_p">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="m_c1_on_positive_to_m">
      <value value="2.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="p_c1_on_negative_accepted_from_p">
      <value value="-10"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="p_c1_deviation">
      <value value="20"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="p_c1_on_neutral_from_m">
      <value value="2.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="p_c2_deviation">
      <value value="33.3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="p_c1_noise_deviation">
      <value value="1.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="p_c1_on_idle">
      <value value="-0.1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="m_c2_on_positive_from_p">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="m_c2_on_negative_to_p">
      <value value="-10"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="Action_Threshold" repetitions="10" runMetricsEveryStep="true">
    <setup>setup</setup>
    <go>go</go>
    <timeLimit steps="5000"/>
    <metric>mean [ c1 ] of turtles</metric>
    <metric>mean [ c2 ] of turtles</metric>
    <enumeratedValueSet variable="p_c1_on_neutral_to_m">
      <value value="2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="m_c2_on_neutral_from_p">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="stealth">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="m_c2_on_neutral_to_m">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="p_c2_on_positive_to_p">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="p_c2_on_positive_from_p">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="p_c2_on_idle">
      <value value="-0.1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="negative_threshold">
      <value value="15"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="m_c1_on_negative_to_m">
      <value value="-10"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="margin_size">
      <value value="10"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="p_c2_on_neutral_from_m">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="population">
      <value value="250"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="m_c1_on_neutral_to_p">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="p_c1_noise_mean">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="p_c1_on_positive_to_m">
      <value value="2.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="action_threshold">
      <value value="0"/>
      <value value="10"/>
      <value value="20"/>
      <value value="30"/>
      <value value="40"/>
      <value value="50"/>
      <value value="60"/>
      <value value="70"/>
      <value value="80"/>
      <value value="90"/>
      <value value="100"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="m_c1_on_positive_from_p">
      <value value="5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="p_c2_on_negative_accepted_from_p">
      <value value="-10"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="p_c2_on_negative_rejected_from_p">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="p_c2_mean">
      <value value="33.3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="p_c1_on_positive_from_p">
      <value value="5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="m_c1_on_negative_accepted_from_m">
      <value value="-10"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="m_c2_on_positive_to_m">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="m_c1_on_positive_to_p">
      <value value="2.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="p_c2_noise_deviation">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="m_c1_on_negative_rejected_from_m">
      <value value="30"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="m_c2_on_negative_accepted_from_p">
      <value value="-10"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="m_c1_on_neutral_from_m">
      <value value="2.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="m_c2_on_negative_rejected_from_p">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="m_c2_mean">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="p_c1_on_neutral_from_p">
      <value value="2.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="p_c1_on_negative_accepted_from_m">
      <value value="-10"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="p_c1_on_negative_rejected_from_m">
      <value value="30"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="m_c2_on_idle">
      <value value="-0.1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="m_c2_on_neutral_to_p">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="m_c2_on_positive_from_m">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="p_c2_on_negative_to_p">
      <value value="-10"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="m_c2_on_negative_to_m">
      <value value="-50"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="p_c2_on_neutral_from_p">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="p_c1_mean">
      <value value="45"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="m_c1_on_idle">
      <value value="-0.1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="p_c1_on_negative_to_m">
      <value value="-10"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="p_c2_on_positive_from_m">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="m_c1_noise_deviation">
      <value value="1.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="m_c2_noise_mean">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="m_c1_deviation">
      <value value="20"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="p_c2_on_neutral_to_m">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="m_c1_on_neutral_to_m">
      <value value="2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="m_c2_deviation">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="m_c1_mean">
      <value value="20"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="m_c1_on_positive_from_m">
      <value value="5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="p_c1_on_neutral_to_p">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="m_c1_noise_mean">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="m_c2_on_positive_to_p">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="m_c1_on_negative_to_p">
      <value value="-5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="m_c2_on_neutral_from_m">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="p_c1_on_positive_from_m">
      <value value="5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="p_c2_on_positive_to_m">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="p_c1_on_positive_to_p">
      <value value="2.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="m_c2_on_negative_accepted_from_m">
      <value value="-50"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="p_c1_on_negative_rejected_from_p">
      <value value="15"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="m_c2_on_negative_rejected_from_m">
      <value value="50"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="positive_threshold">
      <value value="50"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="m_c2_noise_deviation">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="p_c2_noise_mean">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="p_c2_on_negative_accepted_from_m">
      <value value="-50"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="critical_faculty">
      <value value="40"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="p_c2_on_negative_rejected_from_m">
      <value value="50"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="m_c1_on_negative_accepted_from_p">
      <value value="-10"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="p_c2_on_negative_to_m">
      <value value="-50"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="p_c1_on_negative_to_p">
      <value value="-5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="m_c1_on_negative_rejected_from_p">
      <value value="15"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="m_c1_on_neutral_from_p">
      <value value="2.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="p_c2_on_neutral_to_p">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="m_c1_on_positive_to_m">
      <value value="2.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="p_c1_on_negative_accepted_from_p">
      <value value="-10"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="p_c1_deviation">
      <value value="20"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="p_c1_on_neutral_from_m">
      <value value="2.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="p_c2_deviation">
      <value value="33.3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="p_c1_noise_deviation">
      <value value="1.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="p_c1_on_idle">
      <value value="-0.1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="m_c2_on_positive_from_p">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="m_c2_on_negative_to_p">
      <value value="-10"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="Positive_Threshold" repetitions="10" runMetricsEveryStep="true">
    <setup>setup</setup>
    <go>go</go>
    <timeLimit steps="5000"/>
    <metric>mean [ c1 ] of turtles</metric>
    <metric>mean [ c2 ] of turtles</metric>
    <enumeratedValueSet variable="p_c1_on_neutral_to_m">
      <value value="2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="m_c2_on_neutral_from_p">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="stealth">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="m_c2_on_neutral_to_m">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="p_c2_on_positive_to_p">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="p_c2_on_positive_from_p">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="p_c2_on_idle">
      <value value="-0.1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="negative_threshold">
      <value value="15"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="m_c1_on_negative_to_m">
      <value value="-10"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="margin_size">
      <value value="10"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="p_c2_on_neutral_from_m">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="population">
      <value value="250"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="m_c1_on_neutral_to_p">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="p_c1_noise_mean">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="p_c1_on_positive_to_m">
      <value value="2.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="action_threshold">
      <value value="66.6"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="m_c1_on_positive_from_p">
      <value value="5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="p_c2_on_negative_accepted_from_p">
      <value value="-10"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="p_c2_on_negative_rejected_from_p">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="p_c2_mean">
      <value value="33.3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="p_c1_on_positive_from_p">
      <value value="5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="m_c1_on_negative_accepted_from_m">
      <value value="-10"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="m_c2_on_positive_to_m">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="m_c1_on_positive_to_p">
      <value value="2.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="p_c2_noise_deviation">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="m_c1_on_negative_rejected_from_m">
      <value value="30"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="m_c2_on_negative_accepted_from_p">
      <value value="-10"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="m_c1_on_neutral_from_m">
      <value value="2.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="m_c2_on_negative_rejected_from_p">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="m_c2_mean">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="p_c1_on_neutral_from_p">
      <value value="2.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="p_c1_on_negative_accepted_from_m">
      <value value="-10"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="p_c1_on_negative_rejected_from_m">
      <value value="30"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="m_c2_on_idle">
      <value value="-0.1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="m_c2_on_neutral_to_p">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="m_c2_on_positive_from_m">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="p_c2_on_negative_to_p">
      <value value="-10"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="m_c2_on_negative_to_m">
      <value value="-50"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="p_c2_on_neutral_from_p">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="p_c1_mean">
      <value value="45"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="m_c1_on_idle">
      <value value="-0.1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="p_c1_on_negative_to_m">
      <value value="-10"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="p_c2_on_positive_from_m">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="m_c1_noise_deviation">
      <value value="1.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="m_c2_noise_mean">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="m_c1_deviation">
      <value value="20"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="p_c2_on_neutral_to_m">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="m_c1_on_neutral_to_m">
      <value value="2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="m_c2_deviation">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="m_c1_mean">
      <value value="20"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="m_c1_on_positive_from_m">
      <value value="5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="p_c1_on_neutral_to_p">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="m_c1_noise_mean">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="m_c2_on_positive_to_p">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="m_c1_on_negative_to_p">
      <value value="-5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="m_c2_on_neutral_from_m">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="p_c1_on_positive_from_m">
      <value value="5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="p_c2_on_positive_to_m">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="p_c1_on_positive_to_p">
      <value value="2.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="m_c2_on_negative_accepted_from_m">
      <value value="-50"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="p_c1_on_negative_rejected_from_p">
      <value value="15"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="m_c2_on_negative_rejected_from_m">
      <value value="50"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="positive_threshold">
      <value value="0"/>
      <value value="10"/>
      <value value="20"/>
      <value value="30"/>
      <value value="40"/>
      <value value="50"/>
      <value value="60"/>
      <value value="70"/>
      <value value="80"/>
      <value value="90"/>
      <value value="100"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="m_c2_noise_deviation">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="p_c2_noise_mean">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="p_c2_on_negative_accepted_from_m">
      <value value="-50"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="critical_faculty">
      <value value="40"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="p_c2_on_negative_rejected_from_m">
      <value value="50"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="m_c1_on_negative_accepted_from_p">
      <value value="-10"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="p_c2_on_negative_to_m">
      <value value="-50"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="p_c1_on_negative_to_p">
      <value value="-5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="m_c1_on_negative_rejected_from_p">
      <value value="15"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="m_c1_on_neutral_from_p">
      <value value="2.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="p_c2_on_neutral_to_p">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="m_c1_on_positive_to_m">
      <value value="2.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="p_c1_on_negative_accepted_from_p">
      <value value="-10"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="p_c1_deviation">
      <value value="20"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="p_c1_on_neutral_from_m">
      <value value="2.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="p_c2_deviation">
      <value value="33.3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="p_c1_noise_deviation">
      <value value="1.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="p_c1_on_idle">
      <value value="-0.1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="m_c2_on_positive_from_p">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="m_c2_on_negative_to_p">
      <value value="-10"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="Negative_Threshold" repetitions="10" runMetricsEveryStep="true">
    <setup>setup</setup>
    <go>go</go>
    <timeLimit steps="5000"/>
    <metric>mean [ c1 ] of turtles</metric>
    <metric>mean [ c2 ] of turtles</metric>
    <enumeratedValueSet variable="p_c1_on_neutral_to_m">
      <value value="2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="m_c2_on_neutral_from_p">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="stealth">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="m_c2_on_neutral_to_m">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="p_c2_on_positive_to_p">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="p_c2_on_positive_from_p">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="p_c2_on_idle">
      <value value="-0.1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="negative_threshold">
      <value value="0"/>
      <value value="10"/>
      <value value="20"/>
      <value value="30"/>
      <value value="40"/>
      <value value="50"/>
      <value value="60"/>
      <value value="70"/>
      <value value="80"/>
      <value value="90"/>
      <value value="100"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="m_c1_on_negative_to_m">
      <value value="-10"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="margin_size">
      <value value="10"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="p_c2_on_neutral_from_m">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="population">
      <value value="250"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="m_c1_on_neutral_to_p">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="p_c1_noise_mean">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="p_c1_on_positive_to_m">
      <value value="2.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="action_threshold">
      <value value="66.6"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="m_c1_on_positive_from_p">
      <value value="5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="p_c2_on_negative_accepted_from_p">
      <value value="-10"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="p_c2_on_negative_rejected_from_p">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="p_c2_mean">
      <value value="33.3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="p_c1_on_positive_from_p">
      <value value="5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="m_c1_on_negative_accepted_from_m">
      <value value="-10"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="m_c2_on_positive_to_m">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="m_c1_on_positive_to_p">
      <value value="2.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="p_c2_noise_deviation">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="m_c1_on_negative_rejected_from_m">
      <value value="30"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="m_c2_on_negative_accepted_from_p">
      <value value="-10"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="m_c1_on_neutral_from_m">
      <value value="2.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="m_c2_on_negative_rejected_from_p">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="m_c2_mean">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="p_c1_on_neutral_from_p">
      <value value="2.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="p_c1_on_negative_accepted_from_m">
      <value value="-10"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="p_c1_on_negative_rejected_from_m">
      <value value="30"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="m_c2_on_idle">
      <value value="-0.1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="m_c2_on_neutral_to_p">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="m_c2_on_positive_from_m">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="p_c2_on_negative_to_p">
      <value value="-10"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="m_c2_on_negative_to_m">
      <value value="-50"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="p_c2_on_neutral_from_p">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="p_c1_mean">
      <value value="45"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="m_c1_on_idle">
      <value value="-0.1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="p_c1_on_negative_to_m">
      <value value="-10"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="p_c2_on_positive_from_m">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="m_c1_noise_deviation">
      <value value="1.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="m_c2_noise_mean">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="m_c1_deviation">
      <value value="20"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="p_c2_on_neutral_to_m">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="m_c1_on_neutral_to_m">
      <value value="2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="m_c2_deviation">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="m_c1_mean">
      <value value="20"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="m_c1_on_positive_from_m">
      <value value="5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="p_c1_on_neutral_to_p">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="m_c1_noise_mean">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="m_c2_on_positive_to_p">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="m_c1_on_negative_to_p">
      <value value="-5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="m_c2_on_neutral_from_m">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="p_c1_on_positive_from_m">
      <value value="5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="p_c2_on_positive_to_m">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="p_c1_on_positive_to_p">
      <value value="2.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="m_c2_on_negative_accepted_from_m">
      <value value="-50"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="p_c1_on_negative_rejected_from_p">
      <value value="15"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="m_c2_on_negative_rejected_from_m">
      <value value="50"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="positive_threshold">
      <value value="50"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="m_c2_noise_deviation">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="p_c2_noise_mean">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="p_c2_on_negative_accepted_from_m">
      <value value="-50"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="critical_faculty">
      <value value="40"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="p_c2_on_negative_rejected_from_m">
      <value value="50"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="m_c1_on_negative_accepted_from_p">
      <value value="-10"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="p_c2_on_negative_to_m">
      <value value="-50"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="p_c1_on_negative_to_p">
      <value value="-5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="m_c1_on_negative_rejected_from_p">
      <value value="15"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="m_c1_on_neutral_from_p">
      <value value="2.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="p_c2_on_neutral_to_p">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="m_c1_on_positive_to_m">
      <value value="2.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="p_c1_on_negative_accepted_from_p">
      <value value="-10"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="p_c1_deviation">
      <value value="20"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="p_c1_on_neutral_from_m">
      <value value="2.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="p_c2_deviation">
      <value value="33.3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="p_c1_noise_deviation">
      <value value="1.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="p_c1_on_idle">
      <value value="-0.1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="m_c2_on_positive_from_p">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="m_c2_on_negative_to_p">
      <value value="-10"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="Critical_faculty" repetitions="10" runMetricsEveryStep="true">
    <setup>setup</setup>
    <go>go</go>
    <timeLimit steps="5000"/>
    <metric>mean [ c1 ] of turtles</metric>
    <metric>mean [ c2 ] of turtles</metric>
    <enumeratedValueSet variable="p_c1_on_neutral_to_m">
      <value value="2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="m_c2_on_neutral_from_p">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="stealth">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="m_c2_on_neutral_to_m">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="p_c2_on_positive_to_p">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="p_c2_on_positive_from_p">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="p_c2_on_idle">
      <value value="-0.1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="negative_threshold">
      <value value="15"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="m_c1_on_negative_to_m">
      <value value="-10"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="margin_size">
      <value value="10"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="p_c2_on_neutral_from_m">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="population">
      <value value="250"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="m_c1_on_neutral_to_p">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="p_c1_noise_mean">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="p_c1_on_positive_to_m">
      <value value="2.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="action_threshold">
      <value value="66.6"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="m_c1_on_positive_from_p">
      <value value="5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="p_c2_on_negative_accepted_from_p">
      <value value="-10"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="p_c2_on_negative_rejected_from_p">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="p_c2_mean">
      <value value="33.3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="p_c1_on_positive_from_p">
      <value value="5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="m_c1_on_negative_accepted_from_m">
      <value value="-10"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="m_c2_on_positive_to_m">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="m_c1_on_positive_to_p">
      <value value="2.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="p_c2_noise_deviation">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="m_c1_on_negative_rejected_from_m">
      <value value="30"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="m_c2_on_negative_accepted_from_p">
      <value value="-10"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="m_c1_on_neutral_from_m">
      <value value="2.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="m_c2_on_negative_rejected_from_p">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="m_c2_mean">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="p_c1_on_neutral_from_p">
      <value value="2.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="p_c1_on_negative_accepted_from_m">
      <value value="-10"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="p_c1_on_negative_rejected_from_m">
      <value value="30"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="m_c2_on_idle">
      <value value="-0.1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="m_c2_on_neutral_to_p">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="m_c2_on_positive_from_m">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="p_c2_on_negative_to_p">
      <value value="-10"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="m_c2_on_negative_to_m">
      <value value="-50"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="p_c2_on_neutral_from_p">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="p_c1_mean">
      <value value="45"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="m_c1_on_idle">
      <value value="-0.1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="p_c1_on_negative_to_m">
      <value value="-10"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="p_c2_on_positive_from_m">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="m_c1_noise_deviation">
      <value value="1.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="m_c2_noise_mean">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="m_c1_deviation">
      <value value="20"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="p_c2_on_neutral_to_m">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="m_c1_on_neutral_to_m">
      <value value="2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="m_c2_deviation">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="m_c1_mean">
      <value value="20"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="m_c1_on_positive_from_m">
      <value value="5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="p_c1_on_neutral_to_p">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="m_c1_noise_mean">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="m_c2_on_positive_to_p">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="m_c1_on_negative_to_p">
      <value value="-5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="m_c2_on_neutral_from_m">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="p_c1_on_positive_from_m">
      <value value="5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="p_c2_on_positive_to_m">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="p_c1_on_positive_to_p">
      <value value="2.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="m_c2_on_negative_accepted_from_m">
      <value value="-50"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="p_c1_on_negative_rejected_from_p">
      <value value="15"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="m_c2_on_negative_rejected_from_m">
      <value value="50"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="positive_threshold">
      <value value="50"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="m_c2_noise_deviation">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="p_c2_noise_mean">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="p_c2_on_negative_accepted_from_m">
      <value value="-50"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="critical_faculty">
      <value value="0"/>
      <value value="10"/>
      <value value="20"/>
      <value value="30"/>
      <value value="40"/>
      <value value="50"/>
      <value value="60"/>
      <value value="70"/>
      <value value="80"/>
      <value value="90"/>
      <value value="100"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="p_c2_on_negative_rejected_from_m">
      <value value="50"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="m_c1_on_negative_accepted_from_p">
      <value value="-10"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="p_c2_on_negative_to_m">
      <value value="-50"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="p_c1_on_negative_to_p">
      <value value="-5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="m_c1_on_negative_rejected_from_p">
      <value value="15"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="m_c1_on_neutral_from_p">
      <value value="2.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="p_c2_on_neutral_to_p">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="m_c1_on_positive_to_m">
      <value value="2.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="p_c1_on_negative_accepted_from_p">
      <value value="-10"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="p_c1_deviation">
      <value value="20"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="p_c1_on_neutral_from_m">
      <value value="2.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="p_c2_deviation">
      <value value="33.3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="p_c1_noise_deviation">
      <value value="1.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="p_c1_on_idle">
      <value value="-0.1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="m_c2_on_positive_from_p">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="m_c2_on_negative_to_p">
      <value value="-10"/>
    </enumeratedValueSet>
  </experiment>
</experiments>
@#$#@#$#@
@#$#@#$#@
default
0.0
-0.2 0 0.0 1.0
0.0 1 1.0 0.0
0.2 0 0.0 1.0
link direction
true
0
Line -7500403 true 150 150 90 180
Line -7500403 true 150 150 210 180

big_arrow
0.0
-0.2 0 0.0 1.0
0.0 1 1.0 0.0
0.2 0 0.0 1.0
link direction
true
0
Polygon -7500403 true true 150 150 45 210 60 240 165 180 150 150
Polygon -7500403 true true 150 150 150 150 135 180 240 240 255 210
@#$#@#$#@
1
@#$#@#$#@
