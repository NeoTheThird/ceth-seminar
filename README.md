# ComputationalEthics

Members of marginalized communities are often accused of being “too sensitive” when it comes to supposedly harmless acts of microaggression. This model simulates a society consisting of marginalized and non-marginalized agents that interact with each other and may or may not perform acts of microaggression based on their individual convictions. Agents witnessing a microaggression might condone, ignore or condemn such microaggressions, thus potentially influencing a perpetrator's conviction.

This model has been the subject of the following paper published on arXiv:

- [Johannah Sprinz, 2022, **"Y'all are just too sensitive": A computational ethics approach to understanding how prejudice against marginalized communities becomes epistemic belief**, arXiv, doi: 10.48550/arXiv.2207.01017](https://doi.org/10.48550/arXiv.2207.01017)

The model can be run [on the web](https://netlogoweb.org/web?https://neothethird.gitlab.io/ceth-seminar/model.nlogo).

[![Screenshot of the NetLogo model UI](./screenshot.png)](https://netlogoweb.org/web?https://neothethird.gitlab.io/ceth-seminar/model.nlogo)

## Setup

### Simulation

- Download [NetLogo](https://ccl.northwestern.edu/netlogo/download.shtml)

#### NetLogo (GUI)

- Start NetLogo, open `model.nlogo`, and follow the embedded documentation

#### BehavioralSpace (headless)

- `<path to netlogo>/netlogo-headless.sh --model model.nlogo --experiment "Margin_size" --table "./results/model Margin_size-table.csv"`

### Plotting

- Install [Miniconda](https://docs.conda.io/en/latest/miniconda.html)
- `conda activate ceth`
- `pip install -r requirements.txt`
- `jupyter lab`

## Conventions

- Git will strip artifacts from jupyter notebooks before committing. Run `nbstripout *.ipynb` to do this manually.

## Credits

(c) [Matthias Fruth](https://de.linkedin.com/in/matthias-fruth-825711180) and [Johannah Sprinz](https://spri.nz/) 2022

Original development within the [Master's practical seminar "Computational Ethics" at LMU Munich in the winter term of 2021](https://uni2work.ifi.lmu.de/course/W21/IfI/CEth), supervised by [Prof. Dr. François Bry](https://www.pms.ifi.lmu.de/mitarbeiter/derzeitige/francois-bry/index.html).
